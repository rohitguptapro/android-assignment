package com.demo.carrentalapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    ArrayList<Car> cList = new ArrayList<>();
    ArrayList<String> cAvail = new ArrayList<>();
    Spinner cars;
    TextView rate,total;
    RadioButton young,middle,old;
    CheckBox chb[] = new CheckBox[3];
    EditText days;
    ImageView cImg;
    Button cal;
    double originalPrice;
    double allTotal=0;
    double insurance=0;
    double seatPerDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fillData();
        cars = findViewById(R.id.spCar);
        rate = findViewById(R.id.txvDailyRate);
        total = findViewById(R.id.txvTotal);
        young = findViewById(R.id.rdb21);
        middle = findViewById(R.id.rdbmiddle);
        old = findViewById(R.id.rdb65);
        cal = findViewById(R.id.btncal);
        young.setOnClickListener(this);
        middle.setOnClickListener(this);
        old.setOnClickListener(this);
        cal.setOnClickListener(this);
        chb[0] = findViewById(R.id.chb0);
        chb[1] = findViewById(R.id.chb1);
        chb[2] = findViewById(R.id.chb2);
        days = findViewById(R.id.edtDays);
        cImg = findViewById(R.id.imgCar);

        ArrayAdapter aa = new ArrayAdapter(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,cAvail);
        //ArrayAdapter aa=new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,pTypes);
        cars.setAdapter(aa);
        cars.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                rate.setText(String.valueOf(cList.get(i).getDailyRate()));
                originalPrice=cList.get(i).getDailyRate();
                cImg.setImageResource(cList.get(i).getImage());
                young.setChecked(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        for(CheckBox cb:chb)
            cb.setOnCheckedChangeListener(new CheckBoxAction());
    }

    public void fillData() {
        cList.add(new Car("Hyundai", 5000.76, R.drawable.hyundai, 1));
        cList.add(new Car("Mahindra", 6000.34, R.drawable.mahindra, 1));
        cList.add(new Car("Maruti", 7032.21, R.drawable.maruti, 1));
        cList.add(new Car("Renault", 8096.02, R.drawable.renault, 1));
        cList.add(new Car("Tata", 5560.65, R.drawable.tata, 1));
        cList.add(new Car("Toyota", 6050.89, R.drawable.toyota, 0));

        for (Car car : cList)
            if (car.getStatus() == 1)
                cAvail.add(car.getName());
    }

    @Override
    public void onClick(View view) {
        double currentPrice=Double.parseDouble(rate.getText().toString());
        switch (view.getId()){
            case R.id.rdb21:
                insurance= 15;break;
            case R.id.rdbmiddle:
                insurance= 7;break;
            case R.id.rdb65:
                insurance= 10;break;
            case R.id.btncal:
                int noOfDays = Integer.parseInt(days.getText().toString());
                currentPrice += insurance + seatPerDay * noOfDays;
                allTotal = currentPrice;
                total.setText(String.format("%.2f",allTotal));
        }
    }

    private class CheckBoxAction implements CompoundButton.OnCheckedChangeListener{


        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            double currentPrice=Double.parseDouble(rate.getText().toString());
            //int noOfDays = Integer.parseInt(days.getText().toString());
            double perDay=0;
            if(compoundButton.getId()==R.id.chb0) {
                if (chb[0].isChecked())
                    perDay += 7;
                else
                    perDay -= 7;
            }
            if(compoundButton.getId()==R.id.chb1) {
                if (chb[1].isChecked())
                    perDay += 5;
                else
                    perDay -= 5;
            }
            if(compoundButton.getId()==R.id.chb2) {
                if (chb[2].isChecked())
                    perDay += 15;
                else
                    perDay -= 15;
            }
            seatPerDay = perDay;
            //currentPrice += perDay + insurance;
            //allTotal = currentPrice;

        }

    }

}