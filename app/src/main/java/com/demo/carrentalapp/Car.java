package com.demo.carrentalapp;

public class Car {
    private String name;
    private double dailyRate;
    private int image;
    private  int status;

    public Car(String name, double dailyRate, int image, int status) {
        this.name = name;
        this.dailyRate = dailyRate;
        this.image = image;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public double getDailyRate() {
        return dailyRate;
    }

    public int getImage() {
        return image;
    }

    public int getStatus() {
        return status;
    }
}
